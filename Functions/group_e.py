"""
This module contains a class with six methods.

"""

from urllib.request import urlretrieve
import os
import zipfile
import warnings
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np

warnings.filterwarnings("ignore")


class BikeDataset:
    """
    Downloads a dataset from a zip file and reads it. Then, analyzes
    correlations and plots data in a time series.

    Attributes
    ----------
    link: string
        A string containing the url of the file to download.
        Default is the url for the Bike-Sharing-Dataset.zip file.
    zip_file: string
        A string containing the name of the zip file.
        Default is the 'Bike-Sharing-Dataset.zip' file.
    directory: string
        A string containing the directory to download the files to.
        Default is a Downloads folder.
    output_file: string
        A string containing the name of the output file.
        Default value is 'hour.csv'.
    data: DataFrame
        The Pandas DataFrame corresponding to the dataset.

    Methods
    ----------
    download()
        Downloads the zip file into a Downloads folder under
        the current directory, if it doesn't exist.
    read()
        Reads a particular csv file within the zip file. 
        Assigns the data to a Pandas DataFrame.
        Converts the DataFrame index to hold hourly datetime information.
    corr()
        Plots a correlation matrix between specific variables.
    week()
        Generates a plot that analyzes bike rentals through time.
    month()
        Returns a bar chart of the average total of rentals by month.
    forecast()
        Returns a graph of 168-hour period (a Mon-Sun week) corresponding 
        to the mean number of bicycle rentals, with a confidence interval. 
    """

    def __init__(
        self,
        link: str = "https://archive.ics.uci.edu/ml/machine-learning-databases/00275/Bike-Sharing-Dataset.zip",
        zip_file: str = "Bike-Sharing-Dataset.zip",
        directory: str = "../Downloads",
        output_file: str = "hour.csv",
    ):

        """
        Defines attributes of classes of type Bike.

        Returns
        ----------
        Nothing
        """
        self.link = link
        self.directory = directory
        self.zip_file = zip_file
        self.output_file = output_file

    def download(self):

        """
        Downloads the .zip file with the data into a downloads folder
        in the root directory of the project (main project directory).
        If the .zip file already exists, the method will not download it again.

        Returns
        ----------
        Nothing
        """

        if not os.path.exists(self.directory + "/" + self.zip_file):
            urlretrieve(self.link, self.directory + "/" + self.zip_file)
        else:
            print("File already exists!")

    def read(self):

        """
        Reads the .csv file with hourly aggregations inside the .zip file
        in the downloads folder into
        a pandas dataframe which will become an attribute of the Class.

        Returns
        ----------
        Nothing. Prints a string if FileNotFoundError.
        """
        try:
            zip_contents = zipfile.ZipFile(self.directory + "/" + self.zip_file)
            data = pd.read_csv(zip_contents.open(self.output_file))
            data["hourly_datetime"] = (
                data["dteday"].astype(str) + "-" + data["hr"].astype(str)
            )
            data.index = pd.to_datetime(data.hourly_datetime, format="%Y-%m-%d-%H")
            self.data = data
        except FileNotFoundError:
            print("Call the 'download' method first!")

    def corr(self):

        """
        Plots a correlation matrix of month, humidity, weather situation, temperature,
        windspeed, and total number of bike rentals.

        Returns
        ----------
        A heatmap representing a correlation matrix.
        """

        try:
            correlation = self.data[
                ["hum", "mnth", "weathersit", "temp", "windspeed", "cnt"]
            ]
            correlation = correlation.corr()

            plt.figure(figsize=(20, 8))

            heat_map = sns.heatmap(
                correlation,
                vmin=-1,
                vmax=1,
                center=0,
                cmap=sns.diverging_palette(20, 220, n=200),
                square=True,
                annot=True,
                annot_kws={"size": 15},
            )

            heat_map.set_xticklabels(heat_map.get_xticklabels(), rotation=0)

            heat_map.set_yticklabels(
                heat_map.get_yticklabels(), rotation=90, va="center"
            )

            plt.title("Correlation Matrix", size=20)
            plt.show()

        except:
            print("There is no data to read. Call the 'read' method first!")

    def week(self, number_week: int):

        """
        Allows you to pick a week number between 0 and 102 weeks.
        If the chosen number is not in the [0, 102] range
        the method should raise a ValueError and warn the user of the allowed range.
        If everything is OK, the method should then plot the chosen week.
        Use 'instant' for x and 'cnt' for y.

        Parameters
        ----------
        number_week: integer
            An integer defining the week number.

        Returns
        ----------
        Plot with the number of bycicle Rentals per hour.
        """

        if number_week not in range(0, 103):
            raise ValueError(
                "Week not available. \nPlease input an integer between 0 and 102."
            )

        weeks = []

        for i in range(0, len(self.data)):
            if self.data.index[i].year == 2011:
                nweek = int(self.data.index[i].strftime("%W"))
                weeks.append(nweek)

            if self.data.index[i].year == 2012:
                nweek = int(self.data.index[i].strftime("%W")) + 52
                weeks.append(nweek)

        self.data["nweek"] = weeks

        x_axis = self.data["instant"].where(self.data["nweek"] == number_week)
        y_axis = self.data["cnt"].where(self.data["nweek"] == number_week)

        plt.figure(figsize=(20, 8))
        plt.style.use("seaborn-darkgrid")
        plt.title("Bycicle Rentals per Hour - Week %d" % number_week, size=18)
        plt.xlabel("Instants (Hours)", size=14)
        plt.ylabel("Number of Bycicle Rentals", size=14)
        plt.plot(x_axis, y_axis)
        plt.show()

    def month(self):

        """
        Plots the average total rentals by month of the year.
        
        Returns
        ---------
        A barchart where the x-axis is the month (column 'mnth' of the dataframe) 
        and the y-axis represents the total average rentals as the mean value 
        of bycicle rentals of a given month. The height of the bars corresponds
        to this mean value.
        """

        mnth_mn = self.data[["mnth", "cnt"]].groupby("mnth").mean()
        plt.figure(figsize=(20, 8))
        plt.bar(mnth_mn.index, height=mnth_mn.cnt)

        plt.title("Average total rentals by month of the year", size=18)
        plt.xlabel("Month", size=14)
        plt.ylabel("Total average rentals", size=14)
        plt.grid(b=None)

        plt.show()

    def forecast(self, month_: [int, str]):

        """
        This method allows you to pick a month of the year,
        to conduct a forecast.
        the method should print a message and warn the user of the allowed values.
        If everything is OK, the method should then plot the chosen month.
               
        Parameters
        ------------
        month: integer or string
            An integer defining the month choosen for forecast
            or a String with the month's name.

        Returns
        ------------
        Plots a 168-hour period (a Mon-Sun week) corresponding 
        to the average rental with a shaded area corresponding 
        to an interval of -1, +1 standard deviation 
        """

        try:
            month_string = []

            for i in np.arange(len(self.data)):
                month_s = self.data.index[i].strftime("%B")
                month_string.append(month_s)

            self.data["Month"] = month_string

            monthly_data = self.data

            if isinstance(month_, int):
                monthly_data = self.data[self.data["mnth"] == month_]
            elif isinstance(month_, str):
                monthly_data = self.data[self.data["Month"] == month_]

            monthly_data["hour_of_the_week"] = (
                monthly_data.index.dayofweek * 24 + monthly_data["hr"]
            )
            month_stats = monthly_data.groupby(["hour_of_the_week"]).cnt.agg(
                ["mean", "std"]
            )

            fig, a_x = plt.subplots(figsize=(20, 8))
            a_x.fill_between(
                month_stats.index,
                month_stats["mean"] - month_stats["std"],
                month_stats["mean"] + month_stats["std"],
                alpha=0.4,
            )

            month_stats["mean"].plot(ax=a_x, c="C3")
            a_x.set_ylabel("Number of Rentals", size=15)
            a_x.set_xlabel("Hour of the Week", size=15)
            plt.title(f"Expected weekly rentals in: {month_}", loc="center", size=20)
            plt.tight_layout()
            plt.show()

        except:
            print(
                """Invalid input! Please input an integer between 1 and 12 or 
                a string with the name of the month using starting with an upper case."""
            )
