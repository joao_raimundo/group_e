# Group_E

This project aims to analyze the mobility of bicycle users, in order to retrieve useful insights
to promote bicycle awareness and, with it, promote a green transition for this transportation type.

To do so, we are going to analyze the hourly bicycle rentals in the years of 2011 and 2012, the
possible correlations with the weather, using variables like the temperature and humidity, and the
possible seasonality and trend that might exist across a week. We'll also analyze average bicycle rentals 
per month and, finally, we'll try to forecast bicycle rentals using the data in rhe dataset.

For this analysis, we'll make use of a Python Class and the corresponding methods, previously defined.
You can check the code behind this project in the Functions folder. In the Notebooks, we have both the
final notebook with the analysis and conclusions - the 'Bike.ipynb' notebook -, and the prototyping 
notebooks used developing the code in 'group_e.py'. In the Downloads folder you can find the related 
data used. 

Naturally, we needed to create a conda environment in order to have all the required python packages 
installed and ready to be used. You can rapidly create this environment in your shell using the file 
'Group_e_package_list.yml'. 

This project is being developed by Group E, which has 4 members:
- Filipe Ribeiro, 29136, 29136@novasbe.pt
- João Colaço, 44661, 44661@novasbe.pt
- João Raimundo, 45014, 45014@novasbe.pt
- Rita Freches, 45143, 45143@novasbe.pt
